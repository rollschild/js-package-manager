import findUp from "find-up";
import fs from "fs-extra";
import * as yargs from "yargs";

export default async function (args: yargs.Arguments) {
  // Find and read the `package.json`
  // By walking up parent dirs
  const packageJsonPath = (await findUp("package.json"))!;
  const root = await fs.readJson(packageJsonPath);
}
