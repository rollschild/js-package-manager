# js-package-manager

## How to start this project from scratch

1. `cd js-package-manager`
2. `yarn init -2`
3. Add type definitions for packages such as this `yarn add @types/yargs --dev`
4. Add typescript `yarn add --dev typescript`
5. Add editor plugin/support, for me I'm using NeoVim + `coc.nvim`: `yarn dlx @yarnpkg/sdks vim`
6. Add `tsconfig.json` with your TS configs
